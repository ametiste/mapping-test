package com.dph.mapping.test.mvc;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.net.URISyntaxException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath:spring/app-config.xml", "classpath:spring/mapping-mvc.xml" })
public class IntegrationTest {

    @Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;


    @Before
	public void setup() throws IOException, URISyntaxException {


		this.mockMvc = webAppContextSetup(this.wac).build();


	}

	@Test
	public void test() throws Exception {

		this.mockMvc
				.perform(
						get("/data").accept(new MediaType("application", "vnd.com.dph.mapping+json"))
								.characterEncoding("UTF-8"))
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(new MediaType("application", "vnd.com.dph.mapping+json")))
				.andExpect(jsonPath("$.msg").exists())
				.andExpect(jsonPath("$.msg").value("Short"));
	}
	



}
