package com.dph.mapping.test.dto;

/**
 * Created by Daria on 22.07.2014.
 */
public class JustAContainer {

    private String msg;

    public JustAContainer(String msg) {

        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}
