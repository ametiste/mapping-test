package com.dph.mapping.test.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dph.mapping.test.dto.JustAContainer;

/**
 * Created by Daria on 22.07.2014.
 */
@Controller
public class ContentTypeMappedController {


	@RequestMapping(value = "/data", produces = "application/vnd.com.dph.mapping+json")
	@ResponseBody
    public Object getShortContentType() {
        return new JustAContainer("Short");
    }

	@RequestMapping(value = "/data", produces = "application/vnd.com.dph.mapping+json;extendedData=BucketOfKittens")
	@ResponseBody
    public Object getExtendedContentType() {
        return new JustAContainer("Extended");
    }


}
